package edu.school21.printer.app;

import edu.school21.printer.logic.ImagesConverter;
import com.beust.jcommander.JCommander;
import java.util.Arrays;

class Program {
	public static void main(String[] args) {
		
		System.out.println(Arrays.toString(args));

		Args parsedArgs = new Args();
		
		JCommander.newBuilder()
  			.addObject(parsedArgs)
  			.build()
  			.parse(args);

		ImagesConverter converter = new ImagesConverter();

		char result[][] = converter.convert(parsedArgs.filePath, 
			parsedArgs.black.charAt(0), parsedArgs.white.charAt(0));
	}
}

