package edu.school21.printer.app;

import com.beust.jcommander.Parameter;

public class Args {
 
  @Parameter(names = "--filePath")
  public String filePath;

  @Parameter(names = "--white")
  public String white;

  @Parameter(names = "--black")
  public String black;
}