package ex00;

/**
 * 16.10.2020
 * 12. Day 03
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class HenThread extends Thread {
    @Override
    public void run() {
//        System.out.println(Thread.currentThread().getName());
        for (int i = 0; i < 100; i++) {
            System.out.println("Hen");
        }
    }
}
