package ex01;

public class Main {
    public static void main(String[] args) {
        // общий ресурс
        Product product = new Product();
        // первый поток - потребитель
        Consumer consumer = new Consumer(product);
        // второй поток - производитель
        Producer producer = new Producer(product);

        consumer.start();
        producer.start();
    }
}
