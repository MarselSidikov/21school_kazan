package ex01;

public class Consumer extends Thread {
    private final Product product;

    public Consumer(Product product) {
        this.product = product;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (product) {
                while (!product.isProduced()) {
                    System.out.println("Consumer ждет");
                    try {
                        product.wait();
                    } catch (InterruptedException e) {
                        throw new IllegalArgumentException();
                    }
                }
                System.out.println("Cosumer потребил");
                product.consume();
                product.notify();
            }
        }
    }
}
