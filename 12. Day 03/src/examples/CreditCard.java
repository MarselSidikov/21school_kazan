package examples;

public class CreditCard {
    // сумма на кредитной карте
    private int amount;

    public CreditCard(int amount) {
        this.amount = amount;
    }

    public int getAmount() {
        return amount;
    }

    // метод для покупки
    public boolean buy(int cost) {
        // если сумма на карте позволяет купить
        if (cost <= amount) {
            // отнимаете сумму покупки
            this.amount -= cost;
            return true;
        } else {
            System.out.println("НЕТ ДЕНЕГ!");
            return false;
        }
    }
}
