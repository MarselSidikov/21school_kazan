package examples;

public class Human extends Thread {
    private String name;
    private CreditCard card;

    public static final Object mutex = new Object();

    public Human(String name, CreditCard card) {
        this.name = name;
        this.card = card;
    }

    @Override
    public void run() {
        // делаем 1000 шагов цикла
        for (int i = 0; i < 1000; i++) {
            synchronized (mutex) {
                // смотрим, есть ли деньги на карте
                if (card.getAmount() > 0) {
                    // пишем, что собираемся купить
                    System.out.println(name + " покупает");
                    // если покупка удалась
                    if (card.buy(10)) {
                        // купил
                        System.out.println(name + " купил");
                    } else {
                        System.out.println(name + " говорит ЭЭЭЭЭЭ");
                    }
                }
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
