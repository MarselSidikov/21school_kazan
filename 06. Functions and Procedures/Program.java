class Program {

	public static boolean endsWith10(int number) {
		// return number % 100 == 10;
		if (number % 100 == 10) {
			return true;
		} else return false;
	}

	public static void print(int from, int to) {
		for (int i = from; i <= to; i++) {
			System.out.print(i);
		}
	}

	public static void main(String[] args) {
		int number = 7710; // 7710 = 77 * 100 + 10
		boolean result = endsWith10(number);
		System.out.println(result);
		print(1, 10);
	}
}