package ex00;

/**
 * 15.10.2020
 * Day 01
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class User {
    // int = 0
    private Integer id; // null

    private String name;

    private Integer balance;

    public User(Integer id, String name, Integer balance) {
        if (balance <= 0) {
            this.balance = 0;
        } else {
            this.balance = balance;
        }

        this.name = name;
        this.id = id;
    }
}
