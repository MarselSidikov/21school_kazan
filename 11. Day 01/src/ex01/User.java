package ex01;

/**
 * 15.10.2020
 * Day 01
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class User {
    // int = 0
    private Integer id; // null

    private String name;

    private Integer balance;

    public User(String name, Integer balance) {
        if (balance <= 0) {
            this.balance = 0;
        } else {
            this.balance = balance;
        }

        this.name = name;
        this.id = UsersIdGenerator.getInstance().generateId();
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getBalance() {
        return balance;
    }
}
