package ex01;

/**
 * 15.10.2020
 * Day 01
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class UsersIdGenerator {

    private static final UsersIdGenerator instance;

    private int lastGeneratedId;

    // отрабатывает при загрузке класса в виртуальную машину
    // срабатывает один единственный раз
    static {
        instance = new UsersIdGenerator();
    }

    private UsersIdGenerator() {
        this.lastGeneratedId = 1;
    }

    public static UsersIdGenerator getInstance() {
        return instance;
    }

    public int generateId() {
        int result = lastGeneratedId;
        lastGeneratedId++;
        return result;
    }
}
