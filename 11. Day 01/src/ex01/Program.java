package ex01;

import java.util.ArrayList;

/**
 * 15.10.2020
 * Day 01
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Program {
    public static void main(String[] args) {
//        UsersIdGenerator usersIdGenerator = new UsersIdGenerator();
//        UsersIdGenerator usersIdGenerator = UsersIdGenerator.getInstance();
        User marsel = new User("Марсель", 0);
        User igor = new User("Игорь", 100);
        User semyon = new User("Семен", 100);
        System.out.println(marsel.getId() + " " + marsel.getName());
        System.out.println(igor.getId() + " " + igor.getName());
        System.out.println(semyon.getId() + " " + semyon.getName());
    }
}
