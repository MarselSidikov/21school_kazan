package ex03;

import java.util.UUID;

/**
 * 15.10.2020
 * Day 01
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Program {
    public static void main(String[] args) {
        Transaction transaction = new Transaction(UUID.randomUUID().toString(), null, null, Transaction.Category.INCOME);
        transaction.setSum(100);
        Transaction transaction1 = new Transaction(UUID.randomUUID().toString(), null, null, Transaction.Category.OUTCOME);
        transaction1.setSum(-100);
        Transaction transaction2 = new Transaction(UUID.randomUUID().toString(), null, null, Transaction.Category.INCOME);
        transaction2.setSum(100);

        TransactionsList transactionsList = new TransactionsLinkedList();
        transactionsList.add(transaction);
        transactionsList.add(transaction1);
        transactionsList.add(transaction2);

        transactionsList.print();
    }
}
