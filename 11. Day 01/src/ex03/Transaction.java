package ex03;

import ex00.User;

/**
 * 15.10.2020
 * Day 01
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Transaction {

    public enum Category {
        INCOME, OUTCOME
    }

    private String id; // UUID

    private User recipient;

    private User sender;

    private Category category;

    private Integer sum;

    private Transaction next;

    public Transaction(String id, User recipient, User sender, Category category) {
        this.id = id;
        this.recipient = recipient;
        this.sender = sender;
        this.category = category;
    }

    public Integer getSum() {
        return sum;
    }

    public void setSum(Integer sum) {
        if (this.category == Category.OUTCOME && sum <= 0) {
            this.sum = sum;
        } else if (this.category == Category.INCOME && sum >= 0) {
            this.sum = sum;
        } else {
            this.sum = 0;
        }
    }

    public Transaction getNext() {
        return next;
    }

    public void setNext(Transaction next) {
        this.next = next;
    }
}
