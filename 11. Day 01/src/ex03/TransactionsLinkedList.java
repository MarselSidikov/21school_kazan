package ex03;

/**
 * 15.10.2020
 * Day 01
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class TransactionsLinkedList implements TransactionsList {

    // достаточно знать первый элемент, чтобы иметь доступ ко всему списку
    private Transaction head;
    private int count;

    @Override
    public void add(Transaction transaction) {
        if (head != null) {
            transaction.setNext(head);
        }
        // первый элемент списка - всегда новая транзакция
        head = transaction;
        count++;
    }

    @Override
    public void delete(String uuid) {

    }

    @Override
    public Transaction[] toArray() {
        return new Transaction[0];
    }

    @Override
    public void print() {
        Transaction current = head;
        while (current != null) {
            System.out.println(current.getSum());
            current = current.getNext();
        }
    }
}
