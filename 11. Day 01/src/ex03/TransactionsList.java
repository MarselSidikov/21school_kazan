package ex03;

/**
 * 15.10.2020
 * Day 01
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface TransactionsList {
    void add(Transaction transaction);
    void delete(String uuid);
    Transaction[] toArray();

    // для наглядности
    void print();
}
