package ex02;

/**
 * 15.10.2020
 * Day 01
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface UsersList {
    void add(User user);

    User get(Integer id);

    User getByIndex(Integer index);

    int size();
}
