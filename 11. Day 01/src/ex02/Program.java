package ex02;

/**
 * 15.10.2020
 * Day 01
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Program {
    public static void main(String[] args) {
        UsersList usersList = new UsersArrayList();

        User user = new User("Марсель" , 0);

        // процедура, void
        usersList.add(user);

        // функция, она User
        User userFromList = usersList.get(1);
    }
}
