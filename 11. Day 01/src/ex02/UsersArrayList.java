package ex02;

/**
 * 15.10.2020
 * Day 01
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class UsersArrayList implements UsersList {

    private User users[];
    private int count;

    public UsersArrayList() {
        this.users = new User[10];
        this.count = 0;
    }

    @Override
    public void add(User user) {
        if (count == users.length) {
            // создали новый массив, размер которого в полтора раза больше
            User newArray[] = new User[users.length + (users.length >> 1)];
            // скопировали в него все данные
            for (int i = 0; i < count; i++) {
                newArray[i] = users[i];
            }
            // используем его вместо старого
            users = newArray;
        }
        this.users[count] = user;
        count++;
    }

    @Override
    public User get(Integer id) {
        // пробегаем всех пользователей
        for (int i = 0; i < count; i++) {
            // если нашли пользователя с нужным id
            // users[i].getId() == id
            // equals - проверка на эквивалентность ссылочных типов
            if (users[i].getId().equals(id)) {
                return users[i];
            }
        }

        // сюда попали если не нашли
        throw new UserNotFoundException();
    }

    @Override
    public User getByIndex(Integer index) {
        return users[index];
    }

    @Override
    public int size() {
        return count;
    }
}
