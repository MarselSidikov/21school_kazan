package models;

import java.util.StringJoiner;

/**
 * 20.10.2020
 * 16. Databases
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Car {
    private Integer id;
    private String model;
    private String number;
    private String color;

    public Car(Integer id, String model, String number, String color) {
        this.id = id;
        this.model = model;
        this.number = number;
        this.color = color;
    }

    public Integer getId() {
        return id;
    }

    public String getModel() {
        return model;
    }

    public String getNumber() {
        return number;
    }

    public String getColor() {
        return color;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Car.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("model='" + model + "'")
                .add("number='" + number + "'")
                .add("color='" + color + "'")
                .toString();
    }
}
