import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import models.Account;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * 19.10.2020
 * 16. Databases
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setUsername("postgres");
        hikariConfig.setPassword("qwerty007");
        hikariConfig.setDriverClassName("org.postgresql.Driver");
        hikariConfig.setJdbcUrl("jdbc:postgresql://localhost:5432/school21_db");
        hikariConfig.setMaximumPoolSize(20);
        HikariDataSource dataSource = new HikariDataSource(hikariConfig);
        try {
            Connection connection = dataSource.getConnection();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

        AccountRepository accountRepository = new AccountRepositoryImpl(dataSource);
//        Account account = accountRepository.findById(2);
//        System.out.println(account);

        List<Account> accounts = accountRepository.findAll(0, 6);
        System.out.println(accounts);
    }
}
