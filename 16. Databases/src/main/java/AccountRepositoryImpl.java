import models.Account;
import models.Car;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 19.10.2020
 * 16. Databases
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class AccountRepositoryImpl implements AccountRepository {

    //language=SQL
    private static final String SQL_FIND_BY_ID = "select * from account where id = ?";

    //language=SQL
    private static final String SQL_FIND_ALL = "with paginated_account as (select * " +
            "                         from account order by id " +
            "                         limit ? offset ?) " +
            "select pa.id as _a_id, c.id as _c_id, * from paginated_account pa left join car c on pa.id = c.owner_id";

    private DataSource dataSource;

    public AccountRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Account findById(Integer id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_BY_ID);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                Integer accountId = resultSet.getInt("id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                Integer age = resultSet.getInt("age");

                return new Account(accountId, firstName, lastName, age);
            }
            return null;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException throwables) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException throwables) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException throwables) {
                }
            }
        }
    }

    @Override
    public List<Account> findAll(int page, int size) {

        Map<Integer, Account> accountsMap = new HashMap<>();

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_ALL);
            statement.setInt(1, size);
            statement.setInt(2, page * size);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Integer accountId = resultSet.getInt("_a_id");

                if (!accountsMap.containsKey(accountId)) {
                    String firstName = resultSet.getString("first_name");
                    String lastName = resultSet.getString("last_name");
                    Integer age = resultSet.getInt("age");
                    Account account = new Account(accountId, firstName, lastName, age);
                    accountsMap.put(accountId, account);
                }


                // необходимо получить id текущей машины
                Integer carId = resultSet.getObject("_c_id", Integer.class);

                if (carId != null) {
                    Car car = new Car(carId, resultSet.getString("model"),
                            resultSet.getString("number"), resultSet.getString("color"));
                    accountsMap.get(accountId).getCars().add(car);
                }
            }
            return new ArrayList<>(accountsMap.values());
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException throwables) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException throwables) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException throwables) {
                }
            }
        }
    }
}
