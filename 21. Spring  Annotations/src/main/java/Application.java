import ru.school21.front.Front;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 23.10.2020
 * 20. Spring
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Application {
    public static void main(String[] args) {
//        UsersRepository usersRepository = new UsersRepositoryFakeImpl();
//        UsersService usersService = new UsersServiceImpl(usersRepository);
//        Front ru.school21.front = new FrontConsoleImpl(usersService);

        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
        Front front = context.getBean(Front.class);
        front.run();
    }
}
