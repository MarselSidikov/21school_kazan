package ru.school21.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 23.10.2020
 * 20. Spring
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Component
public class MailsServiceImpl implements MailsService {

    @Value("${mailservice.prefix}")
    private String prefix;

    @Override
    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    @Override
    public void sendMail(String email, String text) {
        System.out.println(prefix + " сообщение <" +
                text + ">" + "было доставлено на <" + email + ">");
    }
}
