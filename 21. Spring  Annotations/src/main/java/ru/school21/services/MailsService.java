package ru.school21.services;

/**
 * 23.10.2020
 * 20. Spring
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface MailsService {
    void setPrefix(String prefix);
    void sendMail(String email, String text);
}
