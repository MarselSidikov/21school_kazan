class Program {
	public static void main(String[] args) {
		// -2^31 ... 2^31 - 1
		int x = 5;
		// -2^63 ... 2^63 - 1
		long y = 1234L;
		System.out.println(x + 1);
	}
}