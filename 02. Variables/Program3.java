class Program3 {
	public static void main(String[] args) {
		// int m = 5;

		// double > int
		// double n = m / 2;

		double m = 5;
		double n = 2;
		// int(4 bytes) < double(8 bytes)
		int c = (int)(m / n);
		System.out.println(c);
	}
}