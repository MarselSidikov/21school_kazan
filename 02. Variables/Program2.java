class Program2 {
	public static void main(String[] args) {
		int m = 17;
		int n = 5;
		int k = m / n; // k = 3
		int t = m % n; // t = 2
		// m = n * k + t
		// 17 = 3 * 5 + 2
		double d = m / n; // d = 3.0
		System.out.println(d);

		d = m * 1.0 / n;
		System.out.println(d);
	}
}