package annotations.runtime;

/**
 * 22.10.2020
 * 19.Annotations
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        SqlGenerator generator = new SqlGenerator();
        String sql = generator.createTableFor(User.class);
        System.out.println(sql);
    }
}
