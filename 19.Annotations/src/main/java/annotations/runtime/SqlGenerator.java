package annotations.runtime;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

/**
 * 22.10.2020
 * 19.Annotations
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class SqlGenerator {
    public <T> String createTableFor(Class<T> aClass) {
        String tableName = "";
        String columnName = "";
        Annotation annotations[] = aClass.getAnnotations();
        for (Annotation annotation : annotations) {
            if (annotation.annotationType().equals(OrmEntity.class)) {
                tableName = ((OrmEntity)annotation).table();
            }
        }

        Field fields[] = aClass.getDeclaredFields();

        for (Field field : fields) {
            Annotation[] fieldsAnnotations = field.getDeclaredAnnotations();

            for (Annotation annotation : fieldsAnnotations) {
                if (annotation.annotationType().equals(OrmColumn.class)) {
                    columnName = ((OrmColumn)annotation).columnName();
                }
            }
        }

        return "create table " + tableName + " ( " + columnName + " integer" + ")";
    }
}
