package annotations.runtime;

/**
 * 22.10.2020
 * 19.Annotations
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@OrmEntity(table = "users")
public class User {
    @OrmColumn(columnName = "height_of_user")
    private Integer height;
}
