package annotations.source;

import com.google.auto.service.AutoService;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;

/**
 * 10.07.2020
 * anno
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@AutoService(Processor.class)
@SupportedAnnotationTypes(value = {"annotations.source.HtmlForm", "annotations.source.HtmlInput"})
public class HtmlProcessor extends AbstractProcessor {

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        // получаем элементы с аннотацией anno.HtmlForm
        Set<? extends Element> annotatedElements = roundEnv.getElementsAnnotatedWith(HtmlForm.class);
        // проходим все эти элементы
        for (Element element : annotatedElements) {
            // получаем путь
            String path = HtmlProcessor.class.getProtectionDomain().getCodeSource().getLocation().getPath();

            // создаете путь к HTML-файлу
            path = path.substring(1, path.length()) + element.getSimpleName().toString() + ".html";
            // получаем путь для записи
            Path out = Paths.get(path);
            try {
                processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, path);
                // записываем в файл
                BufferedWriter writer = new BufferedWriter(new FileWriter(out.toFile()));
                writer.write("<form action='" + element.getAnnotation(HtmlForm.class).action() + "'>");
                writer.close();
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }
        return true;
    }
}

