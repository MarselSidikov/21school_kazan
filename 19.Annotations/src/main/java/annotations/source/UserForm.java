package annotations.source;

/**
 * 10.07.2020
 * anno
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@HtmlForm(formName = "user_form.html", action = "/users")
public class UserForm {
    @HtmlInput(name = "first_name")
    private String firstName;

    @HtmlInput(name = "last_name")
    private String lastName;
}
