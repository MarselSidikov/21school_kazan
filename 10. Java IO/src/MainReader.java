import java.io.FileReader;
import java.io.Reader;
import java.util.Arrays;

/**
 * 14.10.2020
 * 10. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainReader {
    public static void main(String[] args) throws Exception {
        Reader reader = new FileReader("input.txt");
//        int character = reader.read();
//        System.out.println((char)character);

        char chars[] = new char[100];
        int length = reader.read(chars);
        System.out.println(Arrays.toString(chars));
        String message = String.valueOf(chars, 0, length);
        System.out.println(message);
    }
}
