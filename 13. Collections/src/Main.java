import java.util.*;

public class Main {

    public static void main(String[] args) {
        // список - доступ к элементам по индексам
        List<String> stringsList;

        // stringsList = new ArrayList<>(); // список на основе массива, очень быстро - получение элемента по индексу
        stringsList = new LinkedList<>(); // список на основе связаного списка, очень быстро - вставка в начало и в конец, очень медленно получение

        stringsList.add("Hello!");
        stringsList.add("Hello!");
        stringsList.add("Hello!");
        System.out.println(stringsList.get(0));

        Set<String> stringsSet = new HashSet<>(); // нельзя получить элемент по индексу
        stringsSet.add("Hello!");
        stringsSet.add("Hello!");

        Map<String, Integer> stringIntegerMap = new HashMap<>();
        stringIntegerMap.put("Марсель", 26);
        stringIntegerMap.put("Станислав", 30);
        stringIntegerMap.put("Марсель", 27);
        System.out.println(stringIntegerMap.get("Марсель"));

        Deque<String> stringDeque = new LinkedList<>();
        stringDeque.push("Hello");
        stringDeque.add("Hello");
        stringDeque.pop();
        stringDeque.poll();

        List<Human> humans = new ArrayList<>();

        humans.add(new Human("Марсель", 26));
        humans.add(new Human("Станислав", 30));
        humans.add(new Human("Игорь", 38));
        humans.add(new Human("Рафис", 34));
        humans.add(new Human("Айгуль", 18));

        Collections.sort(humans);

        System.out.println(humans);

//        Iterator<String> stringIterator = stringsList.iterator();
//        while (stringIterator.hasNext()) {
//            System.out.println(stringIterator.next());
//        }

        for (String line : stringsList) {
            System.out.println(line);
        }
    }
}
