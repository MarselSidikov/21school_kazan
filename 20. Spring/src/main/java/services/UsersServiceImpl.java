package services;

import models.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import repositories.UsersRepository;

import java.time.LocalDateTime;

/**
 * 23.10.2020
 * 20. Spring
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class UsersServiceImpl implements UsersService {

    private UsersRepository usersRepository;

    private MailsService mailsService;

    private PasswordEncoder passwordEncoder;

    public UsersServiceImpl(UsersRepository usersRepository, MailsService mailsService,
                            PasswordEncoder passwordEncoder) {
        this.usersRepository = usersRepository;
        this.mailsService = mailsService;
        this.passwordEncoder = passwordEncoder;
    }

    public void signIn(String login, String password) {
        User user = usersRepository.findByLogin(login);

        if (user != null) {
            if (passwordEncoder.matches(password, user.getPassword())) {
                user.setLastAccessDateTime(LocalDateTime.now());
                usersRepository.update(user);
                mailsService.sendMail(user.getEmail(), "Был выполнен вход");
            } else throw new IllegalArgumentException("Bad user");
        } else throw new IllegalArgumentException("Bad user");
    }

    @Override
    public void signUp(String login, String password, String email) {
        User user = new User(email, login, passwordEncoder.encode(password));
        usersRepository.save(user);
    }
}
