package repositories;

import models.User;

import java.util.List;

/**
 * 23.10.2020
 * 20. Spring
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class UsersRepositoryFakeImpl implements UsersRepository {

    @Override
    public User findByLogin(String login) {
        if (login.equals("Marsel")) {
            return new User("sidikov.marsel@gmail.com", "Marsel", "qwerty007");
        }
        return null;
    }

    @Override
    public void save(User user) {
        System.out.println("Сохранен: " + user);
    }

    @Override
    public List<User> findAll() {
        return null;
    }

    @Override
    public void update(User user) {

    }
}
