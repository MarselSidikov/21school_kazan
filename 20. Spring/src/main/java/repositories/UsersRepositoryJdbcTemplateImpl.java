package repositories;

import models.User;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * 23.10.2020
 * 20. Spring
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class UsersRepositoryJdbcTemplateImpl implements UsersRepository {

    //language=SQL
    private static final String SQL_FIND_BY_LOGIN = "select * from account where login = ?";

    //language=SQL
    private static final String SQL_INSERT = "insert into account (email, login, password) values (?, ?, ?);";

    private JdbcTemplate jdbcTemplate;

    public UsersRepositoryJdbcTemplateImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private RowMapper<User> userRowMapper = (row, i) ->
            new User(row.getString("email"),
            row.getString("login"),
            row.getString("password"));

    @Override
    public User findByLogin(String login) {
        return jdbcTemplate.queryForObject(SQL_FIND_BY_LOGIN, userRowMapper, login);
    }

    @Override
    public void save(User user) {
        jdbcTemplate.update(SQL_INSERT, user.getEmail(), user.getLogin(), user.getPassword());
    }

    @Override
    public List<User> findAll() {
        return jdbcTemplate.query("select * from account", userRowMapper);
    }

    @Override
    public void update(User user) {

    }
}
