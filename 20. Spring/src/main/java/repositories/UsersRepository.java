package repositories;

import models.User;

import java.util.List;

/**
 * 23.10.2020
 * 20. Spring
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface UsersRepository {
    User findByLogin(String login);

    void save(User user);

    List<User> findAll();

    void update(User user);
}
