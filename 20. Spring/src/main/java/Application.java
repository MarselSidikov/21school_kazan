import front.Front;
import front.FrontConsoleImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import repositories.UsersRepository;
import repositories.UsersRepositoryFakeImpl;
import services.UsersService;
import services.UsersServiceImpl;

/**
 * 23.10.2020
 * 20. Spring
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Application {
    public static void main(String[] args) {
//        UsersRepository usersRepository = new UsersRepositoryFakeImpl();
//        UsersService usersService = new UsersServiceImpl(usersRepository);
//        Front front = new FrontConsoleImpl(usersService);

        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
        Front front = context.getBean("front", Front.class);
        front.run();
    }
}
