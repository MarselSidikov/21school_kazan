package front;

import services.UsersService;

import java.util.Scanner;

/**
 * 23.10.2020
 * 20. Spring
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class FrontConsoleImpl implements Front {
    private UsersService usersService;

    public FrontConsoleImpl(UsersService usersService) {
        this.usersService = usersService;
    }

    public void run() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите пожалуйста email, логин, пароль");
        String newEmail = scanner.nextLine();
        String newLogin = scanner.nextLine();
        String newPassword = scanner.nextLine();
        usersService.signUp(newLogin, newPassword, newEmail);
        System.out.println("Введите пожалуйста логин и пароль");
        String login = scanner.nextLine();
        String password = scanner.nextLine();
        usersService.signIn(login, password);
    }
}
