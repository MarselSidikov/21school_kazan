# Занятие 01.

## Вывод

```
System.out.println(...);
```

## Переменные

* int - целые (операции с целыми числами - +, -, \*, /, %)

* long - большие целые (пригодятся в задании)

* double - числа с плавающей точкой (дроби)

## Преобразования

* неявное преобразование - от меньшего к большему , например int -> double

* явное преобразование - от большего к меньшему с потерей данных doubule -> int

## Структуры управления

* Линейная - операторы вполняются друг за другом.

* Условие - выполняет операторы только при истинности условия.

* Цикл - операторы повторяются по какому-либо условию. Каждый проход цикла называется итерацией.

## Считывание данных

* `scanner.nextLine()` - считывает строку целиком в String

* `scanner.next()` - считывает строку до первого разделителя (пробел или конец строки)

* После `System.out.println()` нужно вызвать `scanner.nextLine()` если требуется дальше считывать с консоли данные.

# Занятие 02.

## Система контроля версий git

* `git add ИМЯ_ФАЙЛА` - добавление файла в текущий коммит.

* `git log` - просмотр истории коммитов.

* `git diff ИМЯ_ФАЙЛА` - посмотреть, какие изменения были внесены.

* `Коммит` - фиксация набора изменений.

* `git commit -m 'комментарий'` - создание коммита.

* `git push` - отправка коммитов на сервер.

* `git pull` - получение изменений с сервера.

* `git rm -rf --cached .` - необходимо сделать после добавления gitignore.

## Основы ООП

* `Human marsel = new Human();` - создание объекта, `Human` - тип, класс. `marsel` - объектная переменная, `new` - выделение памяти, `Human()` - конструктор.

* Наследование - позволяет копировать состояние и переопределять/копировать поведение объектов классов-предков объектами классов-потомков.

* Восходящее преобразование - объектная переменная класса-предка может ссылаться на объект класса-потомка.

* Абстрактный класс - класс, на основе которых нельзя создать объект.

* Абстрактный метод - метод, который не имеет реализации. Возможен только в абстрактных классах.

* Полиморфизм - возможность работать с объектами разных типов так, как будто это один и тот же тип. 

* Интерфейс - контракт, требование к поведению классов, имплементирующих данный интерфейс.

## Java IO

* Поток (IO Stream) - последовательность байтов.

* По умолчанию в Java открываются три потока - `System.in`, `System.out`, `System.err`.

* `InputStream` (см. `OutputStream`) - абстрактный класс, описывающий поток ввода. Подходит для чтения аудио-видео-изображений.

* `FileInputStream` (см. `FileOutputStream`) - потомок `InputStream`, конкретная реализация входного потока на основе файла. 

* `Reader` (см. `Writer`)- абстрактный класс, описывающий поток ввода в символах.

## Многопоточность

* `a.join()` - означает, что поток, из которого вызвали `join()` будет ждать полного завершения потока `a`.

## Коллекции

* `List` - интерфейс-список. Нужен, чтобы хранить элементы и иметь информацию об их порядке (индексе). Реализации - `ArrayList` и `LinkedList`.

* `Set` - интерфейс-множество. Нужен, чтобы хранить уникальные элементы. Не имеет доступа по индексу. Реализация - `HashSet`.

* `Deque` - интерфейс-двусторонняя очерель (стек и очередь). Реализация - `LinkedList`.

* `Map` - интерфейс-словарь. Может хранить ключи (уникальные) и значения. Реализация `HashMap`.

## Сборка JAR-архива "руками"

* Компиляция с использованием пакетов

```
javac -d target/ src/java/edu/school21/printer/*/*.java
```

* Компиляция с использованием сторонней библиотеки

```
javac -cp lib/jcommander-1.78.jar -d target/ src/java/edu/school21/printer/*/*.java
```

* Перенос сторонней библиотеки в папку `target`

```
cp lib/jcommander-1.78.jar target/
```

* Разархивируем jar-архив сторонней библиотеки в папку `com`

```
jar xf jcommander-1.78.jar com
```

* Сборка jar-архива с использованием manifes-файла с указанием входной точки приложения

```
jar -cvfm printer.jar ../src/manifest.txt .
```

- `c` - нужно создать новый архив

- `v` - `verbose` - нужно выводить логи сборки

- `f` - вы указываете целевое название архива

- `m` - вы говорите, что будете использовать свой manifest-файл и назване + расположение этого файла будет параметром сборки

- `.` - в архив нужно поместить все файлы из текущей директории

* Посмотреть содержимое архива

```
jar tf printer.jar
```

* Запуск архива

```
java -jar printer.jar --filePath "C:/image.jpg" --white 0 --black 1
```

## Работа с базами данных

* Создание базы данных

```SQL
create database название_базы_данных;
```

* Создание схемы

```SQL
create schema public;
```

* Запросы

```SQL
create table account
(
    id         serial primary key,
    first_name varchar(20),
    last_name  varchar(20),
    age        integer not null default 1
);

insert into account(first_name, last_name, age)
values ('Марсель', 'Сидиков', 26);
insert into account(first_name, last_name, age)
values ('Рафис', 'Тухватуллин', 34);
insert into account(first_name, last_name, age)
values ('Руслан', 'Хабибрахманов', 33);


-- запрос на получение всех данных из таблицы
select *
from account;

-- запрос на получение имен и возраста + сортировка по убыванию возрасту
-- если возраст совпал, то по возрастанию имени
select first_name, age
from account
order by age desc, first_name;

-- получение по условию
select *
from account
where age > 30;

-- обновление записей
update account
set age = 27
where id = 1;
update account
set age = 27
where last_name = 'Сидиков';

create table car
(
    id       serial primary key,
    model    varchar(100),
    color    varchar(100),
    owner_id int,
    foreign key (owner_id) references account (id)
);

insert into car(model, color, owner_id)
VALUES ('BMW', 'black', null),
       ('Lada Calina', 'black', 2),
       ('Cherry', 'grey', 3);

alter table car
    add number varchar(20) not null default 'A001AA';

-- подзапрос на получение водителя
select first_name, age
from account
where id in (
    select owner_id
    from car
    where color = 'black');

-- пересечение таблиц
select *
from account a
         inner join car c on a.id = c.owner_id;

select *
from account a
         left join car c on a.id = c.owner_id;

select *
from account a
         right join car c on a.id = c.owner_id;

select *
from account a
         full outer join car c on a.id = c.owner_id;



-- drop table car;
-- drop table account;
-- delete
-- from account
-- where id = 2;
```