import java.util.Scanner;
class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String text = scanner.nextLine();

		char array[] = text.toCharArray();

		int counts[] = new int[128];

		for (int i = 0; i < array.length; i++) {
			// `A` -> 65
			// counts[65]++ -> counts[65] = 3
			counts[array[i]]++;
		}

		System.out.printf("%10s|%5d|  |%6s\n", "Hello", 10, "Marsel");
	}
}