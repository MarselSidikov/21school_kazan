package ru.school21;

import java.util.ArrayList;
import java.util.List;

/**
 * 21.10.2020
 * 17. JUnit
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class NumbersProcessor {

    private Parser parser;

    public NumbersProcessor(Parser parser) {
        this.parser = parser;
    }

    public List<Boolean> checkEven(String numbers[]) {
        List<Boolean> result = new ArrayList<>();
        for (int i = 0; i < numbers.length; i++) {
            result.add(parser.parse(numbers[i]) % 2 == 0);
        }
        return result;
    }
}
