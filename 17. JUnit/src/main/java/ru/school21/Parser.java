package ru.school21;

/**
 * 21.10.2020
 * 17. JUnit
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface Parser {
    int parse(String string);
}
