class Program {
	public static void main(String[] args) {
		Human marsel = new Human("Марсель", "Сидиков", 26);
		Human igor = new Human("Игорь", "Фамилия", 18);
		marsel.setAge(-10); // marsel.age -> 1
		System.out.println(marsel.getAge());
		// marsel.age = -10;
		// igor.age = 20; // нельзя
		/*
		marsel.firstName = "Марсель";
		marsel.lastName = "Сидиков";
		marsel.age = 26;
		*/

	}
}