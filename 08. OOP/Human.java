class Human {
	// поле, field
	private String firstName;
	private String lastName;
	private int age;

	public Human(String firstName, String lastName, int age) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.setAge(age);
	}

	public void setAge(int age) {
		if (age >= 0) {
			this.age = age;
		} else {
			this.age = 1;
		}
	}

	public int getAge() {
		return age;
	}

	// TODO: set/get for firstName, lastName
}