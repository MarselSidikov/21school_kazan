import java.util.Scanner;

class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		String message = scanner.next();
		int number = scanner.nextInt();
		String name = scanner.next();
		// scanner.nextLine(); scanner.next();

		String message1 = scanner.next();
		int number1 = scanner.nextInt();
		String name1 = scanner.next();

		System.out.println(message + " " + number + " " + name);
		System.out.println(message1 + " " + number1 + " " + name1);
		scanner.nextLine();
		
		String line = scanner.nextLine();

		String parsed[] = line.split(",");

		System.out.println(parsed[0]);
		System.out.println(parsed[1]);
		System.out.println(parsed[2]);
	}
}