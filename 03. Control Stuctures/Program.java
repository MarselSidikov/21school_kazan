import java.util.Scanner;

class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int numbersCount = scanner.nextInt();
		int evenNumbersSum = 0;

		int i = 0;

		while (i < numbersCount) {
			int number = scanner.nextInt();
			if (number % 2 == 0) {
				evenNumbersSum = evenNumbersSum + number;
			}
			
			i++;
		}


		System.out.println(evenNumbersSum);
	}
}