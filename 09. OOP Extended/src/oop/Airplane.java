package oop;

/**
 * 14.10.2020
 * 09. OOP Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Airplane implements Arm {
    @Override
    public void fire() {
        System.out.println("Самолет стреляет!");
    }
}
