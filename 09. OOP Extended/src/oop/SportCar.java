package oop;

/**
 * 14.10.2020
 * 09. OOP Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class SportCar extends Transport {

    private String color;

    public SportCar(double fuelAmount, double fuelConsumption, String color) {
        super(fuelAmount, fuelConsumption);
        this.color = color;
    }

    // переопределение
    public void go(int km) {
        System.out.println("Мой автомобиль имеет " + color + " цвет!!!");
        this.fuelAmount -= (km * fuelConsumption) / 100;
    }

    @Override
    public void stop() {
        System.out.println("Останавились :)");
    }
}
