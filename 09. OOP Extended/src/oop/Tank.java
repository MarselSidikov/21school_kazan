package oop;

/**
 * 14.10.2020
 * 09. OOP Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Tank extends Transport implements Arm {

    private int bulletsCount;

    public Tank(double fuelAmount, double fuelConsumption, int bulletsCount) {
        super(fuelAmount, fuelConsumption);
        this.bulletsCount = bulletsCount;
    }

    public void fire() {
        System.out.println("Танк стреляет!");
        this.bulletsCount -= 1;
    }

    @Override
    public void stop() {
        System.out.println("Танк стоит!");
    }
}
