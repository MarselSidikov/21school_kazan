package oop;

/**
 * 14.10.2020
 * 09. OOP Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Program {
    public static void main(String[] args) {
        Transport transport;
//        transport = new Transport(60, 10);
//        transport.go(5);
//        System.out.println(transport.getFuelAmount());

        SportCar sportCar = new SportCar(80, 20, "черный");
        SportCar sportCar1 = sportCar;
        sportCar.go(10);
        sportCar1.go(15);
        System.out.println(sportCar.getFuelAmount());

        Tank tank = new Tank(100, 15, 10);
        tank.fire();

        transport = sportCar;
//        transport = sportCar1;
//        transport = tank;

        transport.go(10);

        Transport transports[] = {sportCar, tank};

        for (int i = 0; i < transports.length; i++) {
            transports[i].go(10);
            transports[i].stop();
        }

        Airplane airplane = new Airplane();

        Arm arms[] = {tank, airplane};
        arms[0].fire();
        arms[1].fire();
    }
}
