package oop;

/**
 * 14.10.2020
 * 09. OOP Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public abstract class Transport {
    protected double fuelAmount;
    protected double fuelConsumption; // 10

    public Transport(double fuelAmount, double fuelConsumption) {
        this.fuelAmount = fuelAmount;
        this.fuelConsumption = fuelConsumption;
    }

    public void go(int km) {
        this.fuelAmount -= (km * fuelConsumption) / 100;
    }

    public double getFuelAmount() {
        return fuelAmount;
    }

    public abstract void stop();
}
