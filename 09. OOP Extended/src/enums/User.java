package enums;

/**
 * 14.10.2020
 * 09. OOP Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class User {
    private UserState state;

    public User(UserState state) {
        this.state = state;
    }

    public UserState getState() {
        return state;
    }

    public enum State {
        ACTIVE, BANNED;
    }
}
