package enums;

import java.util.Scanner;

/**
 * 14.10.2020
 * 09. OOP Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Program {
    public static void main(String[] args) {
//        UserState state = UserState.BANNED;

        Scanner scanner = new Scanner(System.in);
        String stateAsString = scanner.nextLine();

        UserState state = UserState.valueOf(stateAsString);
        User user = new User(state);
        System.out.println(user.getState().name());
        User.State userState = User.State.ACTIVE;
    }
}
