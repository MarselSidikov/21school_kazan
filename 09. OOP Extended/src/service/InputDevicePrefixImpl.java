package service;

import java.util.Scanner;

/**
 * 14.10.2020
 * 09. OOP Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class InputDevicePrefixImpl extends AbstractInputDeviceScanner implements InputDevice {
    // fields - поля
    private String prefix;
    // конструктор
    public InputDevicePrefixImpl(String prefix) {
        super();
        this.prefix = prefix;
    }

    public String read() {
        String line = scanner.nextLine();
        return prefix + " " + line;
    }
}
