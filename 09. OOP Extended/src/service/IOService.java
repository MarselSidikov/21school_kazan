package service;

/**
 * 14.10.2020
 * 09. OOP Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class IOService {
    private OutputDevice outputDevice;
    private InputDevice inputDevice;

    public IOService(OutputDevice outputDevice, InputDevice inputDevice) {
        this.outputDevice = outputDevice;
        this.inputDevice = inputDevice;
    }

    public void print(String message) {
        outputDevice.print(message);
    }

    public String read() {
        return inputDevice.read();
    }
}
