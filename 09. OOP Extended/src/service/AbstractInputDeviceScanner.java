package service;

import java.util.Scanner;

/**
 * 14.10.2020
 * 09. OOP Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public abstract class AbstractInputDeviceScanner {
    protected Scanner scanner;

    public AbstractInputDeviceScanner() {
        this.scanner = new Scanner(System.in);
    }
}
