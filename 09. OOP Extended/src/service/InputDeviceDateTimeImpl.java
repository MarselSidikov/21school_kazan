package service;

import java.time.LocalDateTime;
import java.util.Scanner;

/**
 * 14.10.2020
 * 09. OOP Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class InputDeviceDateTimeImpl extends AbstractInputDeviceScanner implements InputDevice {

    @Override
    public String read() {
        String line = scanner.nextLine();
        return LocalDateTime.now() + " " + line;
    }
}
