package service;

public class Main {

    public static void main(String[] args) {
//	    InputDevicePrefixImpl inputDevice = new InputDevicePrefixImpl("INPUT: ");
        InputDevice inputDevice = new InputDeviceDateTimeImpl();
        OutputDevice outputDevice = new OutputDevicePrefixImpl("OUTPUT: ");

        IOService ioService = new IOService(outputDevice, inputDevice);

        ioService.print("Hello!");
        System.out.println(ioService.read());

    }
}
