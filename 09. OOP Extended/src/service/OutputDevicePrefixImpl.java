package service;

/**
 * 14.10.2020
 * 09. OOP Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class OutputDevicePrefixImpl implements OutputDevice {
    private String prefix;

    public OutputDevicePrefixImpl(String prefix) {
        this.prefix = prefix;
    }

    public void print(String message) {
        System.out.println(prefix + " " + message);
    }
}
